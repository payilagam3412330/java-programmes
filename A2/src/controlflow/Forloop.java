package controlflow;

public class Forloop {
	public static void main(String[] args) {
		Forloop a1 = new Forloop();
		 //a1.forloop();
		// a1.forloop1();
		//a1.forloop2();
		// a1.forloop3();
		 //a1.forloop4();
		 a1.forloop5();
		// a1.forloop6();
		//a1.forloop7();
		//a1.forloop8();
	}

	private void forloop8() {
		
		
	}

	private void forloop7() {
		for (int i = 0; i < 10; i++) {
			// System.out.println(i);
			if (i == 5) {
				break;
			}
			System.out.println(i);
		}

	}

	private void forloop6() {
		for (int i = 0; i < 10; i++) {
			// System.out.println(i);
			if (i == 5) {
				continue;
			}
			System.out.println(i);
		}
	}

	private void forloop5() {
		for (int i = 0; i <6; i++) {
			for (int no = 1; no < 6; no++) {
				System.out.print(i);
			}
			System.out.println();
		}

	}

	private void forloop4() {
		for (int i = 6; i > 1; i--) {
			for (int no = 10; no >= 1; no--) {
				System.out.print(no + " ");
			}
			System.out.println();
		}

	}

	private void forloop3() {
		for (int i = 0; i < 5; i++) {
			for (int no = 1; no <= 5; no++) {
				System.out.print(no + " ");
				// System.out.print("* ");
			}
			System.out.println();
		}
	}

	private void forloop2() {
		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
		System.out.println();
		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
		System.out.println();
		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
		System.out.println();
		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
		System.out.println();
		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
	}

	private void forloop1() {
		int num = 5; // interview question
		System.out.println(num++);
		System.out.println(++num);
		System.out.println(num);
		byte no = 5; // interview question
		byte no1 = 10;
		int no3 = no + no1;
		System.out.println(no3);

	}

	private void forloop() {

		for (int no = 1; no <= 5; no++) {
			System.out.print(no + " ");
		}
	}
}
