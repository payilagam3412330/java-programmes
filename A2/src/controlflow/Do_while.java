package controlflow;

import java.util.Scanner;

public class Do_while {
	public static void main(String[] args) {
		Do_while a1 = new Do_while();
//	
		a1.do_while();
//	//a1.marks();
//	a1.num();
//	
//}
//
//private void num() {
//	
//	for(int i=5;i<=100;i++) {
////		if(i%5==0 && i%10!=0) {
////			System.out.println(i);
////		}
////		if(i/5==10 || i/5==11) {
////			System.out.println(i);
////		}
//		
//		//my try
//		
//			if(i>=50 && i<=59) {
//				System.out.println(i);
//			}
//			if(i%10==5) {
//				System.out.println(i);
//			}
//		

	}

//private void marks() {
//	int[] marks= {20,30,50,70,50};
//	int count=0;
//	for(int value=0;value < marks.length;value++) {
//		if(marks[value]<35) {
//			count++;
//		}
//		
//	}
//    System.out.println(count);
//	
//}

	private void do_while() {
		//bitwise operator
		System.out.println(4 & 5);
		System.out.println(4 | 5);
		System.out.println(4>>2); //right
		System.out.println(4<<2); //left
		System.out.println(~5); 
		System.out.println(4^5);//capsilon (XOR)

//	int no=1;
//	do
//	{
//		System.out.println(no);
//		no++;
//	}
//	while(no<5);
	}

	// interview question

//	byte no=50;
//	no=(byte) (no*10);
//	System.out.println(no);

}
