package lamda_expression;

import java.util.function.Function;
import java.util.function.Predicate;

public class Lamda_demo {   
   public static void main(String[] args) {
	   Funtional_interface f1=(no1,no2)->{return no1+no2;};
	  System.out.println(f1.num(30,50));                             //abstract method
	  
	  Funtional_interface.add1();                                    //static method
	  f1.add3();                                                     //default method
	   
	   
	   
	 //Function method   //Functional->interface
	   Function<Integer, Integer> v=i->{return i;};   
	   System.out.println(v.apply(45));
	   
	   //predicate method     //Functional ->interface
	   Predicate<Integer> p=i->{if(i<=10)return true;return false;};
	   System.out.println(p.test(15));
		   }
	   }
   
