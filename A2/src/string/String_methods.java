package string;

public class String_methods {
	public static void main(String[] args) {
		String name = "vicky";
		String name1 = "vicky";
		// length method()
		System.out.println(name.length());

		// charAt mathod()
		for (int i = 0; i < name.length(); i++) {
			System.out.println(name.charAt(i));
		}
		// startsWith method()
		System.out.println(name.startsWith("vi"));

		// endsWith method()
		System.out.println(name.endsWith("y"));

		// split method ()
		String date = "06/02/2003";
		String[] s1 = date.split("/");
		for (String string : s1) {
			System.out.println(string);
		}

		// compareTo method()
		System.out.println(name.compareTo(name1));

		// compareToIgnorecase method()
		System.out.println(name.compareToIgnoreCase(name1));

		// concat method()
		System.out.println(name.concat(" R"));
		
		//substring method
		System.out.println(name.substring(3));
		System.out.println(name.substring(0, 3));
		
		//contains method()
		System.out.println(name.contains("vy"));
		
		//equals method()
		System.out.println(name.equals(name1));
		
		//equalsIgnorecase method()
		System.out.println(name.equalsIgnoreCase("viky"));
		
		//isEmpty method()
		String name3="";
		System.out.println(name3.isEmpty());
		
		//indexOf method()
		System.out.println(name.indexOf('k'));
		
		//lastIndexof method()
		System.out.println(name.lastIndexOf('c'));
		
		//toUppercase method()
		System.out.println(name.toUpperCase());
		
		//toLowercase method()
		System.out.println(name.toLowerCase());
		
		//toChararray method()
		char[] ch=name.toCharArray();
		for(int i=0;i<ch.length;i++) {
		System.out.println(ch[i]);
		}
		
		//trim method()
//		String name4=" vicky R ";
//		System.out.println(name4.length());
//		name4=name4.trim();
//		System.out.println(name4.length());
		
	}
}
