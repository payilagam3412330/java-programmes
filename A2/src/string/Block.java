package string;

public class Block {
     //static block
	 static {      
		 System.out.println("static-block");
	 }
	 
	 //non-static block
	 {       
		 System.out.println("non-static block");
	 }
	 
	 //main method
	 public static void main(String[] args) {
		Block b1=new Block();
		System.out.println("main-method");
	}
}
