package Learn_array;

public class LearnArray {
	public static void main(String[] args) {
		LearnArray a1 = new LearnArray();
		//a1.foreachloop();
		// a1.array1();
		a1.array2();
		//a1.reverse();
		//a1.sum();
		

	}

	private void sum() {
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		int total=0;
		int value=0;
		while(value<num.length) {
			total=total+num[value];
			value++;
		}
		System.out.println(total);
		
	}

	private void reverse() {
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		for (int value=9;value>=0;value--) {
			System.out.println(num[value]);
		}

	}

	private void array2() {
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int value = 0; value < num.length; value++) { // print add number
			if (num[value] % 2 != 0) {
				System.out.println(num[value]);
			}
		}

	}

	private void array1() {
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int value = 0; value < num.length; value++) { // print even number
			if (num[value] % 2 == 0) {
				System.out.println(num[value]);
			}
		}
	}

	private void foreachloop() {
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int i : num) {
			System.out.print(i+" ");
		}
	}

}
