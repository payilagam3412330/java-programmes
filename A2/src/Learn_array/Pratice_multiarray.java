package Learn_array;

public class Pratice_multiarray {
	public static void main(String[] args) {
		int[][][] mark = { { { 50, 69, 79, 35, 95 }, { 53, 74, 85, 54, 90 }, { 65, 80, 96, 70, 99 } },
				{ { 70, 59, 71, 49, 99 }, { 78, 74, 85, 54, 90 }, { 65, 80, 96, 70, 99 } },
				{ { 40, 50, 60, 70, 80 }, { 60, 70, 80, 90, 99 }, { 65, 80, 96, 70, 99 } },
				{ { 40, 50, 60, 70, 80 }, { 60, 70, 80, 90, 99 }, { 65, 80, 96, 70, 99 } } };
		for (int row = 0; row < mark.length; row++) {
			for (int col = 0; col < mark[row].length; col++) {
				for (int i = 0; i < mark[row][col].length; i++) {
					System.out.print(mark[row][col][i] + " ");
				}
				System.out.println();
			}
			System.out.println();
		}

	}
}
