package Learn_array;

import java.util.Scanner;

public class Add_array {
public static void main(String[] args) {
	//joining two array program example={1,2};
	                                   //{1,2};={1,2,1,2};
	Scanner obj= new Scanner(System.in);
	  System.out.println("Enter 1st array size");
	  int size1=obj.nextInt();
	  
	  int arr1[]= new int[size1];
	  System.out.println("Enter "+size1+" elements");
	  for(int i=0;i<size1;i++)
	  {
	   arr1[i]=obj.nextInt();
	  }
	  
	  //2nd array
	  System.out.println("Enter 2nd array size");
	  int size2=obj.nextInt();
	  
	  int arr2[]= new int[size2];
	  System.out.println("Enter "+size2+" elements");
	  for(int i=0;i<size2;i++)
	  {
	   arr2[i]=obj.nextInt();
	  }
	  
	  int arr3[]=new int[arr1.length+arr2.length];
	  
	  int j=0;//1//2//3//4
	  for(int i=0;i<arr3.length;i++)//10<10
	  {
	   if(i<arr1.length)//7<5
	   {
	    arr3[i]=arr1[i];
	   }
	   else
	   {
	    arr3[i]=arr2[j++];
	    
	   }
	   
	  }
	  
	  for(int i=0;i<arr3.length;i++)
	  {
	   System.out.print(arr3[i]+" ");
	  }
}
}
