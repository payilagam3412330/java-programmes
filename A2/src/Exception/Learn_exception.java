package Exception;

import java.util.Scanner;

public class Learn_exception {
	public static void main(String[] args) {
		Learn_exception l1 = new Learn_exception();
		Scanner sc = new Scanner(System.in);
		System.out.println("enter 2 numbers:");
		int i = sc.nextInt();
		int j = sc.nextInt();
		l1.divide(i, j);
		l1.add(i, j);
	}

	private void add(int i, int j) {
		System.out.println("add" + (i + j));
	}

	private void divide(int i, int j) {
		try {  //Error  possible area-block
			System.out.println("divide " + (i / j));
			int[] ar = new int[i];
			System.out.println("length" + ar.length);
			for (int k = 0; k < 10; k++) {
				System.out.println(ar[k]);
			}
		}   
		//Error handling area-block
		catch (ArithmeticException a1) {
			System.out.println("try catch");
		} catch (NegativeArraySizeException a3) {
			System.out.println("2nd try catch");
		} 
		catch (Exception e) {
			System.out.println("exception");
		}   
		// code cleaning area -block
		finally {
			System.out.println("finally");
		}
	}
}
