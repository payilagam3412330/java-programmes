package Switchcase;

public class Demoswitch {

		public static void main(String[] args) {
			 Demoswitch s1=new  Demoswitch();
			 s1.names();
			 s1.string();
			 s1.ternary();
			int day = 7;
			int marks = 100;
			
			//ternary operator
			String result = marks > 50 ? "pas" : "fail";
			System.out.println(result);

			if (marks >= 50) {
				System.out.println("pass");
			} else {
				System.out.println("fail");
			}

			// switch

			switch (day) {
			case 1:
				System.out.println("sunday");
				break;
			case 2:
				System.out.println("monday");
				break;
			case 3:
				System.out.println("tuesday");
				break;
			case 4:
				System.out.println("wednesday");
				break;
			case 5:
				System.out.println("thursday");
				break;
			case 6:
				System.out.println("friday");
				break;
			case 7:
				System.out.println("saturday");
				break;
			default:
				System.out.println("invalid day");
				break;
			}

			// if-else

			if (day == 1) {
				System.out.println("sunday");
			} else if (day == 2) {
				System.out.println("monday");
			} else if (day == 3) {
				System.out.println("tuesday");
			} else if (day == 4) {
				System.out.println("wednesday");
			} else if (day == 5) {
				System.out.println("thursday");
			} else if (day == 6) {
				System.out.println("friday");
			} else if (day == 7) {
				System.out.println("saturday");
			} else {
				System.out.println("invalid day");
			}

		}
		private void names() {
			String name = "sownesh";
			switch (name) {
			case "sownesh":{System.out.println("very very good boy");}
			break;
			case "yogeshwaran":{System.out.println("good boy");}
			break;
			case "santhosh":{System.out.println("good boy");}
			break;
			case "rasika":{System.out.println("very very bad girl");}
			break;
			case "vigneshwaran":{System.out.println("good boy");}
			break;
			case "aravindh":{System.out.println("good boy");}
			break;
			case "selvi chechi":{System.out.println("good girl");}
			break;
			case "sangeetha":{System.out.println("good girl");}
			break;
			default:{System.out.println("invalid day");}
			break;	}	
			System.out.println();
			}
		private void string() {
			int value =1;
			switch(value) {
			case 1:{System.out.println("January");}
			break;
			case 2:{System.out.println("February");}
			break;
			case 3:{System.out.println("March");}
			break;
			case 4:{System.out.println("April");}
			break;
			case 5:{System.out.println("May");}
			break;
			case 6:{System.out.println("June");}
			break;
			case 7:{System.out.println("July");}
			break;
			case 8:{System.out.println("August");}
			break;
			case 9:{System.out.println("september");}
			break;
			case 10:{System.out.println("October");}
			break;
			case 11:{System.out.println("November");}
			break;
			case 12:{System.out.println("December");}
			break;
			default:{System.out.println("invalid month or input");}
			break;
			}
			System.out.println();
			}
		private void ternary() {
			int mark = 20;
			String result = mark >= 50 ? "pass" : "fail"; //?=if :=else
			System.out.println(result);
		}



	}

