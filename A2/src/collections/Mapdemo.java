package collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Mapdemo {

	public static void main(String[] args) {
		HashMap hm=new HashMap();
		hm.put("pongal", 80);
		hm.put("idly", 10);
		hm.put("poori", 25);
		hm.put("pongal", 40);
		System.out.println(hm);
		
        System.out.println(hm.get("pongal"));
        
        System.out.println(hm.remove("pongal"));
        System.out.println(hm);
        
        System.out.println(hm.containsKey("poori"));
        
        System.out.println(hm.containsValue(30));
        
        System.out.println(hm.putIfAbsent("pongal", 10));
        
        //key ->idly poori ->keySet
        System.out.println(hm.keySet());
        
        //value ->60,50,40 ->collection
        System.out.println(hm.values());
        
        //key       value         key-value
        //idly       10           Entryset
        //poori      25           Entryset
        System.out.println(hm.entrySet());

		Iterator i = hm.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry me = (Map.Entry) i.next();
			System.out.println(me.getKey());
			System.out.println(me.getValue());

			// add total (increase +5) dish value use this.
			me.setValue((int) me.getValue() + 5);

			// add rate one dish use this.
//        	if(me.getKey().equals("pongal")) {   
//        		me.setValue((int)me.getValue()+5);
//        	}
		}
		System.out.println(hm);
	}

	}


