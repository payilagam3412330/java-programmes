package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Comparater implements Comparator {
	public static void main(String[] args) {
		ArrayList name=new ArrayList();
		name.add("vicky");
		name.add("amma");
		name.add("appa");
		name.add("ramya");
		name.add("dhanush");
		System.out.println(name);
		
		Comparater a1=new Comparater();
		Collections.sort(name,a1);
		System.out.println(name);
	}

	@Override
	public int compare(Object parent, Object parent1) {  //down casting.. parent class object child class object ah change pantrathu.
		String child = (String) parent;
		String child1 = (String) parent1;
		int res=child.compareTo(child1);
		
		//print the name asenting order and desenting order.
		if(res>0) {
			return -1;
		}else if(res<0) {
			return 1;
		}
		return 0;
		
		
		//print the name using length longname to shortname (or) shortname to longname. 
//		if (child.length() < child1.length()) {
//			return -1;
//		} else if (child.length() > child1.length()) {
//			return 1;
//		}
//		return 0;
	}

}
