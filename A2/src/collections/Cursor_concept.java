package collections;

import java.util.ArrayList;
import java.util.Iterator;

public class Cursor_concept {

	public static void main(String[] args) {
		ArrayList name=new ArrayList();
		name.add("vicky");
		name.add("amma");
		name.add("appa");
		name.add("ramya");
		name.add("dhanush");
		name.add("vicky");
		System.out.println(name);
		
		for(Object obj:name) {
			String s1=(String)obj;
			if(s1.startsWith("a"))
				System.out.println(obj);
		}
		
		Iterator i=name.iterator();
		boolean result=i.hasNext();
		while(result) {
			System.out.print(i.next()+" ");
			result=i.hasNext();
		}
	}

}
