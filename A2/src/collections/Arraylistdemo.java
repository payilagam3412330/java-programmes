package collections;

import java.util.ArrayList;

public class Arraylistdemo {

	public static void main(String[] args) {
//		int no=20;
//		Integer i=20; //AutoBoxing
//		int no1=i;    //AutoUnboxing
		
		ArrayList a1=new ArrayList();
		a1.add("vicky");
		a1.add(20);
		a1.add(64.5f);
		a1.add(true);
		System.out.println(a1);
		a1.add("java");   
		System.out.println(a1);
		
		//ArrayList methods
		System.out.println(a1.contains("vicky"));  //check the details.
		System.out.println(a1.get(2));             
		System.out.println(a1.indexOf("java"));
		System.out.println(a1.lastIndexOf("java"));
		System.out.println(a1.remove(4));
		System.out.println(a1);
		
		
		//Add two array
		ArrayList a2=new ArrayList();
		a2.addAll(a1);
		System.out.println(a2);
		a2.add("java");
		a2.add("sql");
		System.out.println(a2);
		
		//Array methods
		System.out.println(a2.containsAll(a1));
//		System.out.println(a2.retainAll(a1));
//		System.out.println(a2);
		System.out.println(a2.removeAll(a1));
		System.out.println(a2);
		
		
		
		
		//Wrapper class
		
		//1.short->Short
		//2.byte->Byte
		//3.int->Integer
		//4.long->Long
		//5.float->Float
		//6.double->Double
		//7.boolean->Boolean
		//8.char->Character
		
		

	}

}
