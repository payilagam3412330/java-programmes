package interview;

import java.util.Scanner;

public class Operators2 {

	public static void main(String[] args) {
		//OR gate
		int no1=20;
		int no2=30;
		System.out.println(no1<no2 || ++no2<no1);
		System.out.println(no2);
		
		
		//AND gate                                                                                                 gate
		int no3=20;
		int no4=30;
		System.out.println(no3<no4 && ++no4>no3);
		System.out.println(no4);
		
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the first number: ");
        double num1 = scanner.nextDouble();
        
        System.out.print("Enter the second number: ");
        double num2 = scanner.nextDouble();
        
        if (num1 < num2) {
            System.out.println("The smaller number is: " + num1);
            System.out.println("The larger number is: " + num2);
        } else if (num1 > num2) {
            System.out.println("The smaller number is: " + num2);
            System.out.println("The larger number is: " + num1);
        } else {
            System.out.println("Both numbers are equal.");
        }
        
        scanner.close();

	}

}
