package interview;

//class.
public class Java_basics {   
     
	//main method
	public static void main(String[] args) {
		
		//print variables
		int myInt = 42;
        boolean myBool = true;
        char myChar = 'A';
        float myFloat = 3.14f;
        double myDouble = 2.71828;

        System.out.println("Integer : " + myInt);
        System.out.println("Boolean : " + myBool);
        System.out.println("Character : " + myChar);
        System.out.println("Float : " + myFloat);
        System.out.println("Double : " + myDouble);
		/*  
		This   
		is   
		multi line   
		comment  
		*/ 
		
		//object creation
		Java_basics JB=new Java_basics();
		
		//print name
		String name="vicky";
		//single line commant and also Inline  commant
		System.out.println("myname is ->"+ name);
		
		//method calling
		JB.add();

	}
    //method creation
	public void add() {
		
	}

}
