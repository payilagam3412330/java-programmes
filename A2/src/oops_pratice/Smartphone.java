package oops_pratice;

public abstract class Smartphone {
	abstract void call(int seconds);

	abstract void sendMessage(); // abstract method

	abstract void receiveCall();

	 public void browse() { // non abstract method
		System.out.println("SmartPhone browsing");
	}

	public Smartphone() // constructor
	{
		System.out.println("Smartphone under development");
	}
}
