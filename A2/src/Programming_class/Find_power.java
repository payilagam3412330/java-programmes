package Programming_class;

public class Find_power {

	public static void main(String[] args) {
		//1sd program
		//1^1 2^2 3^3 4^4 5^5
//		Find_power fp=new Find_power();
//		int no=1;
//		while(no<=10) {
//			fp.find(no,no);
//			no=no+1;
//		}
//
//	}
//
//	private void find(int no, int power) {
//		int res=1;
//		while(power>0) {
//			res=res*no;
//			power=power-1;
//		}
//		System.out.println(res);

		
		
		//2nd program
		// 1^0 2^1 3^2 4^3 5^4
//		Find_power fp = new Find_power();
//		int no = 1;
//		while (no <= 5) {
//			fp.find(no, no-1);
//			no = no + 1;
//		}
//
//	}
//
//	private void find(int no, int power) {
//		int res = 1;
//		while (power > 0) {
//			res = res * no;
//			power = power - 1;
//		}
//		System.out.println(res);
		
		
		//3rd program.
		//1^2 2^3 3^4 4^5 5^6
		Find_power fp=new Find_power();
		int no=1;
		while(no<=5) {
			fp.find(no,no+1);
			no=no+1;
		}

	}

	private void find(int no, int power) {
		int res=1;
		while(power>0) {
			res=res*no;
			power=power-1;
		}
		System.out.println(res);

	}

}
