package Programming_class;

public class Binary_to_decimal {

	public static void main(String[] args) {
		Binary_to_decimal bd=new Binary_to_decimal();
		int binary=110,decimal=0,i=0;
		while(binary>0) {
			int rem=binary%10;
			int result=bd.find_power(2,i);
			decimal=decimal+(rem*result);
			i=i+1;
			binary=binary/10;
		}
		System.out.println(decimal);

	}

	private int find_power(int base, int power) {
		int res=1;
		while(power>0) {
			res=res*base;
			power=power-1;
		}
		return res;
	}

}
