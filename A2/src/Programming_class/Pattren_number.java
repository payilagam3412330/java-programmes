package Programming_class;

public class Pattren_number {

	public static void main(String[] args) {

		Pattren_number pn = new Pattren_number();
		pn.n1();
		System.out.println("------------------------------");
		pn.n2();
		System.out.println("------------------------------");
		pn.n3();
		System.out.println("------------------------------");
		pn.n4();
		System.out.println("------------------------------");
		pn.n5();
		System.out.println("------------------------------");
		pn.n6();
		System.out.println("------------------------------");
		pn.n7();
		System.out.println("------------------------------");
		pn.n8();
		System.out.println("------------------------------");
		pn.n9();
		System.out.println("------------------------------");
		pn.n10();
		System.out.println("------------------------------");
		pn.n11();
		System.out.println("------------------------------");
		pn.n12();
		System.out.println("------------------------------");
		pn.n13();
		System.out.println("------------------------------");
		pn.n14();
		System.out.println("------------------------------");
		pn.n15();
		System.out.println("------------------------------");
		pn.n16();
		System.out.println("------------------------------");
		pn.character();
		System.out.println("------------------------------");
		pn.character1();
		System.out.println("------------------------------");
		pn.character3();
		System.out.println("------------------------------");
		pn.character4();
		System.out.println("------------------------------");

	}

	private void n16() {
		for(int r=1;r<=5;r++) {
			for(int c=1;c<=r;c++) {
				if(r==c||r-c==2|| r==5&&c==1) {
					System.out.print("0 ");
				}else {
					System.out.print("1 ");
				}
			}
			System.out.println();
		}
		
	}

	private void n15() {
		for(int r=1;r<=5;r++) {
			for(int c=1;c<=r;c++) {
				if(c%2==0) {
					System.out.print("1 ");
				}else {
					System.out.print("0 ");
				}
			}
			System.out.println();
		}
	}

	private void n14() {
		for (int r = 5; r >= 1; r--) {
			for (int c = 1; c <= r; c++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6-r; i++) {
				System.out.print("* ");
			}
			
			System.out.println();
		}
		
	}

	private void n13() {
		int no=1;
		for(int r=1;r<=4;r++) {
			for(int c=1;c<=no;c++) {
				System.out.print("* ");
			}
			no+=2;
			System.out.println();
		}
		
	}

	private void n12() {
		for (int r = 5; r >= 1; r--) {
			for (int c = 1; c <= r; c++) {
				System.out.print("  ");
			}
			for (int i = 1; i <= 6-r; i++) {
				System.out.print(i+" ");
			}
			
			System.out.println();
		}
		
	}

	private void n11() {
		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6-r; i++) {
				System.out.print("* ");
			}
			
			System.out.println();
		}
		
	}

	private void n10() {
		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6-r; i++) {
				System.out.print(i+" ");
				//System.out.println("* ");
			}
			
			System.out.println();
		}
		
	}

	private void n9() {
		for (int r = 0; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print("  ");
			}
			for (int i = r; i <= 5; i++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
	}

	private void n8() {
		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print("* ");
			}
			for (int i = r; i <= 5; i++) {
				System.out.print("1 ");
			}
			System.out.println();
		}
		
	}

	private void n7() {
		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				if (r == c) {
					System.out.print("1 ");
				} else
					System.out.print("* ");

			}
			System.out.println();
		}
	
	}

	private void character4() {
		for (char r = 'a'; r <= 'e'; r++) {
			for (char c = 'e'; c >= r; c--) {
				System.out.print(c +" ");
			}
			System.out.println();
		}

	}

	private void character3() {
		for (char r = 'e'; r >= 'a'; r--) {
			for (char c = 'a'; c <= r; c++) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

	private void character1() {
		for (char r = 'a'; r <= 'e'; r++) {
			for (char c = 'a'; c <= r; c++) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

	private void character() {
		for (char r = 'e'; r >= 'a'; r--) {
			for (char c = 'e'; c >= r; c--) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

	private void n6() {
//		int end=1;
//		for(int r=1;r<=5;r++) {
//			for(int c=r;c<=end;c++) {
//				System.out.print(c+" ");
//			}
//			System.out.println();
//			end=end+2;
//		}

		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print((r + c) - 1 + " ");
			}
			System.out.println();
		}

	}

	private void n5() {
		int no = 1;
		for (int r = 1; r <= 5; r++) {
			for (int c = 1; c <= r; c++) {
				System.out.print(no + " ");
				no++;
			}
			System.out.println();
		}

	}

	private void n4() {
		for (int r = 5; r >= 1; r--) {
			for (int c = 5; c >= r; c--) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

	private void n3() {
		
		
		for(int r=1;r<=5;r++) {
			for(int c=1;c<=r;c++) {
				System.out.print(c+" ");
			}
			System.out.println();
		}
		

	}

	private void n2() {
		for (int r = 5; r >= 1; r--) {
			for (int c = 5; c >= 6 - r; c--) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

	private void n1() {

		for (int r = 5; r >= 1; r--) {
			for (int c = 1; c <= r; c++) {
				System.out.print(c + " ");
			}
			System.out.println();
		}

	}

}
