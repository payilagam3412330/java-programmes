package Programming_class;

public class Pattern_2 {

	public static void main(String[] args) {
		Pattern_2 p = new Pattern_2();
		// p.p1();
		// p.p2();
		// p.p3();
		// p.pI();
		// p.pC();
		// p.pO();
		// p.pD();
		// p.pP();
		// p.pB();
		// p.pR();
		// p.pR_2();
		// p.pN();
		// p.pX();
        // p.pY();
		// p.pY_2();
		// p.pV();
		// p.pH();
		// p.pA();
		// p.pA_2();
		// p.pM();
		// p.pS();
		 p.p_number5();
	}

	private void p_number5() {
		for (int row = 1; row <= 7; row++) {
		    for (int col = 1; col <= 7; col++) {

		        if (row == 1 || row == 7 || row == 4 || (col == 1 && row <= 4) || (col == 7 && row > 3)) {
		            System.out.print("* ");
		        } else {
		            System.out.print("  "); // Use two spaces to maintain consistent spacing
		        }

		    }
		    System.out.println();
		}
		System.out.println();

		
	}

	private void pS() {
		for (int row = 1; row <= 7; row++) {
		    for (int col = 1; col <= 7; col++) {
		        if ((row == 1 && col != 1) || (row == 4 && col != 1 && col != 7) || (row == 7 && col != 7)) {
		            System.out.print("* ");
		        } else if (col == 1 && (row == 2 || row == 3)) {
		            System.out.print("* ");
		        } else if (col == 7 && (row == 5 || row == 6)) {
		            System.out.print("* ");
		        } else {
		            System.out.print("  "); // Two spaces for an empty space
		        }
		    }
		    System.out.println();
		}

		
	}

	private void pM() {
		for (int row = 1; row <= 7; row++) {
		    for (int col = 1; col <= 7; col++) {
		        if (col == 1 || col == 7 || (col == row && row <= 4) || (col + row == 8 && row <= 4)) {
		            System.out.print("* ");
		        } else {
		            System.out.print("  "); // Make sure to use two spaces here for consistent spacing.
		        }
		    }
		    System.out.println();
		}
		System.out.println();

		
	}

	private void pA_2() {
		  for (int row = 1; row <= 7; row++) {
	            for (int col = 1; col <= 7; col++) {
	                if ((row + col == 5) || (col - row == 3) || (col == 1 && row > 3) || (col == 7 && row > 3) || row == 4) {
	                    System.out.print("* ");
	                } else {
	                    System.out.print("  "); // Use two spaces for a clearer output.
	                }
	            }
	            System.out.println();
	        }
		
	}

	private void pA() {
	    for (int row = 1; row <= 7; row++) {
            for (int col = 1; col <= 7; col++) {
                if (col == 1 || col == 7 || row == 4 || (row == 1 && col == 4)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
		
	

	private void pH() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || row == 4) {
					System.out.print("* ");

				} else {
					System.out.print("  ");
				}

			}
			System.out.println();

		}
		System.out.println();
		
	}

	private void pV() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col && col <= 4 || col + row == 8 && row <= 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
	}

	private void pY_2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col && col <= 4 || col + row == 8 && row <= 4 || col == 4 && row > 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
	}

	private void pY() {
		 for (int row = 1; row <= 7; row++) {
			 for (int col = 1; col <= 7; col++) {
			 if (row == col && col <= 4 || col + row == 8) {
			 System.out.print("* ");
			 }else {
			 System.out.print("  ");
			 }
			 }
			 System.out.println();
			 }
			 System.out.println();
		
	}

	private void pX() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == col || col + row == 8) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
	}

	private void pN() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (col == 1 || col == 7 || row == col) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();

		}
		System.out.println();
		
	}

	private void pR_2() {
		pD();
		for (int r = 2; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && c == 7) {
						System.out.print("  ");
					} else if (r == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			    System.out.println();
			
			}
			}

		
	

	private void pR() {
		pD();
		for (int r = 2; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				 if(c==1 || r==c) {
					 System.out.print("* ");
				 }else {
					 System.out.print("  ");
				 }
			}
			System.out.println();
		}
		
	}

	private void pB() {
		pD();
		for (int r = 2; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && c == 2) {
						System.out.print("  ");
					} else if (r == 7 && c == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pP() {
        pD();
        for(int r=1;r<=7;r++) {
        	System.out.println("* ");
        }
		
	}

	private void pD() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && c==7) {
						System.out.print("  ");
					} else if (r == 7 && c==7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		
	}

	private void pO() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && (c == 1 || c == 7)) {
						System.out.print("  ");
					} else if (r == 7 && (c == 1 || c == 7)) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pC() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1) {
					if (r == 1 && c == 1) {
						System.out.print("  ");
					} else if (r == 7 && c == 1) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pI() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void p3() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 4) {
					System.out.print("* ");
				} else {
					System.out.print("# ");
				}
			}
			System.out.println();
		}

	}

	private void p2() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7) {
					System.out.print("* ");
				} else {
					System.out.print("# ");
				}
			}
			System.out.println();
		}

	}

	private void p1() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				System.out.print("# ");
			}
			System.out.println();
		}

	}

}
