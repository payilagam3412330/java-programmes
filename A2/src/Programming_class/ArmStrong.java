package Programming_class;

public class ArmStrong {

    public static void main(String[] args) {
        ArmStrong as = new ArmStrong();
        int no = 153, no2 = no, arm = 0;
        int power = as.find_count(no); // Calculate the number of digits in 'no'

        while (no > 0) {
            int base = no % 10; // Extract the last digit
            int out= as.find_power(base, power); // Calculate base^power 
            arm=arm+out; // add to 'arm'
            no=no/ 10; // Remove the last digit
        }
        System.out.println(arm);
        if (no2 == arm) {
            System.out.println("Armstrong number");
        } else {
            System.out.println("Not Armstrong");
        }
    }

    // Calculate base^power
    private int find_power(int base, int power) {
        int result = 1;
         while (power>0) {
            result=result* base;
            power=power-1;
        }
        return result;
    }

    // Calculate the number of digits in a number
    private int find_count(int no) {
        int count = 0;
        while (no > 0) {
            no = no / 10;
            count = count + 1;
        }
        return count;
    }
}
