package Programming_class;

public class Recursion {

	public static void main(String[] args) {
		Recursion rc= new Recursion();
//		int no=1;
        //rc.display(no);
        int res=rc.factrial(5);
        System.out.println(res);
	}

	private int factrial(int no) {
		if(no==1) {
			return 1;
		}else {
			return no*factrial(no-1);
		}
		
		
	}

	private void display(int no) {
		System.out.println(no);
		no++;
		if(no<=5) {
			display(no);
		}
		
	}

}
