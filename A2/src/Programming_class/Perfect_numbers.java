package Programming_class;

public class Perfect_numbers {

	public static void main(String[] args) {
		Perfect_numbers pn=new Perfect_numbers();
		int no=496;
		int sum=pn.sum(no);
		if(no==sum) {
			System.out.println("perfect");
		}else {
			System.out.println("not perfect");
		}
	}

	private int sum(int no) {
		int div=1,sum=0;
		while(div<no) {
			if(no%div==0) {
				sum=sum+div;
			}
			div=div+1;
		}
		return sum;
		
	}

}
