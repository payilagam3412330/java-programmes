package Typecasting;

public class ToString_2 {
	
	private int rno;
	private String name;

	public ToString_2(int rno, String name) {
		this.rno = rno;
		this.name = name;
	}
	
	public String toString() {
		return rno+" "+name;
	}

}
