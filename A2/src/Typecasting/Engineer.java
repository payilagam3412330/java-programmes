package Typecasting;

public class Engineer extends Student {

	public static void main(String[] args) {
		Engineer child=new Engineer();
		Engineer child1=new Engineer();
		String name="vicky";
		child.study();
		child.dopratice();
		System.out.println(child.hashCode());
		System.out.println(child1.hashCode());//object hashcode.
		
		System.out.println(name.hashCode());//string hashcode.
		
		System.out.println(child.equals(child1));//.equals method.
		
	    
		
		//upCasting  //widenCasting   //ImplicitCasting
//		Student parent=(Student)child; //child class object ah parent class object  ah change pantrathu.
//		parent.study();
		
		//downCasting  //NarrowCasting  //explicitCasting
//		Student parent=new Engineer();  //dynamic binding.    
//		Engineer child=(Engineer)parent;//parent class object ah child class object ah change pantrathu.
//		child.dopratice();
//		child.study();
	}

	public void dopratice() {
		System.out.println("dopratice");
		
	}
	

}
